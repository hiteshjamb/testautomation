@demo @onlinehelpdemo
Feature: Demo to check online help

  Scenario: This performs the help test and verifies the help screen is loaded for all EC screens and also verifies BF Name, BF description and screenshot for the corresponding screen.
    Given I login with "sysadmin" user and "sysadmin" password
    When I do help page test for first "10" EC screens
    Then I logout from application
