@demo @pageloaddemo
Feature: Demo to check page load

  Scenario: This performs the pageload test and verifies the page is loaded and toolbar on that page is displayed
    Given I login with "sysadmin" user and "sysadmin" password
    Then I do page load test for first "10" EC screens
    And I logout from application
