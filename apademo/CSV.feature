@demo
Feature: Demo to perform IUD using CSV file

  @readcsv
  Scenario: This test performs insert, update and delete operations on the Harbour Interruption screen in transport module using read data from CSV file "TestCSV.csv"
    Given I login with "sysadmin" user and "sysadmin" password
    When I navigate to Harbour Interruption Screen and load "TestCSV.csv" for navigation data
    And I perform insert, update and delete operations and load "TestCSV.csv" for test data
    Then IUD performed successfully using CSV file for test data
    And I logout from application
