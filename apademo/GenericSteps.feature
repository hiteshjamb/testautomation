@demo @genericsteps
Feature: Demo for Generic Steps

  # Steps are commented in the below scenario as it would take some time to complete and affect the database as well
  Scenario: Generic Step Definition to initiate day on any date
    Given I login with "sysadmin" user and "sysadmin" password
    When I start the initiation process from "SYS.DATE - 1" to "SYS.DATE" to create blank records

  #    When I start the initiation process from "SYS.DATE - 7" to "SYS.DATE + 1" to create blank records
  #    When I start the initiation process from "SYS.DATE " to "SYS.DATE + 1" to create blank records
  #    When I start the initiation process from "SYS.DATE - 365" to "SYS.DATE" to create blank records
  #    When I start the initiation process from "SYS.DATE + 365" to "SYS.DATE + 366" to create blank records
  #    When I start the initiation process from "2014-02-01" to "2014-02-01" to create blank records
  Scenario: One Step Definition to navigate to any screen across EC
    Given I login with "sysadmin" user and "sysadmin" password
    When I navigate to "Daily Price Rate" screen under "EC Sales" module and "Price Determination" submodule
    And I navigate to "Daily Price Rate" screen under "EC Sales" module
    And I navigate to "Daily Price Rate" screen
    And I navigate to "Dashboard" screen
    And I navigate to "Inventory Booked" screen under "EC Revenue" module and "Validation" submodule
    And I logout from application

  Scenario: One Step Definition to navigate to any screen across EC and fill navigator values
    Given I login with "sysadmin" user and "sysadmin" password
    When I navigate to "Well Gas Component Forecast" screen and enter navigation data "data1"
    When I navigate to "Document Tracing" screen and enter navigation data
      | Process From Date | Process To Date | Reporting Period | Type                 | Dataset | Accrual | Process Status | Object Type | Reference | Object               |
      | 2010-01-10        | 2011-01-01      | 2011-01-01       | Data Entry Interface | Capex   | Final   | Provisional    | Facility    |           | " Offshore facility" |
    When I navigate to "Four Eye Approval" screen and enter navigation data
      | Class Name | Updated By | Status |
      | COMPANY    | sysadmin   | Open   |
    And I logout from application

  Scenario: Generate Step Definitions Utility to generate Step Definition
    Given I login with "sysadmin" user and "sysadmin" password
    When I generate one step navigation at "screen" level for "Delivery Component Analysis"
    And I generate one step navigation at "module" level for "EC Production"
    And I generate one step navigation at "db" level
    And I logout from application
