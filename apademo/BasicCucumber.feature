@demo1
Feature: Demo to perform basic cucumber Feature

  @iud
  Scenario: This test performs insert, update and delete operations on the Harbour Interruption screen in transport module
    Given I login with "sysadmin" user and "sysadmin" password
    When I navigate to "Harbour Interruption Screen"
      | From Date  | To Date    | Production Unit | Area     | Facility Class 1 |
      | 2011-01-01 | 2011-01-10 | TS1_EC_PU       | TS1_Area | TS1 Fcty         |
    And I perform insert operation
      | Delay From       | Delay To         | Reason       | Comments |
      | 2011-01-01 00:00 | 2011-01-02 00:00 | *Strong wind | Test     |
    And I perform update operation
      | Reason       | Comments |
      | *Strong wind | Testing1 |
    And I perform delete operation
      | Comments |
      | Testing1 |
    Then IUD performed successfully
    And I logout from application

  @scenariooutline
  Scenario Outline: This test performs insert, update and delete operations on the Harbour Interruption screen in transport module
    Given I login with "sysadmin" user and "sysadmin" password
    When I navigate to Forecast Monthly Screen and enter the values "<From Month>","<To Month>", "<Business Unit>", "<Contract Area>", "<Contract>", "<Nomination Point>", "<Forecast>" as input data
    And I perform insert operation "<Month>","<Nomination Point>", "<Qty>", "<Comments>"
    And I perform update operation "<Nomination Point>","<Comments1>"
    And I perform delete operation "<Comments1>"
    Then verify the inserted record is deleted successfully "<Comments1>"
    And I logout from application

    Examples: 
      | From Month | To Month | Business Unit | Contract Area | Contract       | Nomination Point    | Forecast      | Month   | Qty | Comments | Comments1 |
      | 2011-01    | 2016-10  | SS1 BU        | SS1 CA        | SS1_Contract_A | SS1_CONTRACT_A_DP_A | SS1_FCST_SD_1 | 2016-10 |   5 | Test     | Testing1  |
      | 2011-01    | 2016-10  | SS1 BU        | SS1 CA        | SS1_Contract_A | SS1_CONTRACT_A_DP_A | SS1_FCST_SD_1 | 2016-10 |   8 | Test     | Testing2  |
