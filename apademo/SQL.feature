@demo1 @sql
Feature: Demo to perform IUD using SQL in different format

  Scenario: This test performs navigation on Well Screen using SQL result set and testdata using DB_USERNAME as ECKERNEL
    Given I login with "sysadmin" user and "sysadmin" password
    And I execute "install_test_automation.sql"
    When I navigate to Well Screen
      | Query                                                                                                                                                                                                               |
      | SELECT 'Date' "Attribute_label",TO_CHAR(TO_DATE(SYSDATE,'DD-MM-YY'),'YYYY-MM-DD') "Attribute_value" FROM DUAL UNION ALL select attribute_label, attribute_value from table(zp_test_automation.getNavValues('Well')) |
    And I logout from application

  Scenario: Export Result from sql query to spreadsheet
    Given I login with "sysadmin" user and "sysadmin" password
    When I export the result of sql query to spreadsheet
      | Sql Query                  | Workbook Name   | Sheet Name |
      | select * from t_basis_user | demoResultExcel | demo       |
    Then I verify the generated workbook
      | Workbook Name   |
      | demoResultExcel |
    And I logout from application
