@demo @other
Feature: Demo which include other functionalities in framework

  @screenshotfail
  Scenario: catch screenshot if test fail
    Given I login with "sysadmin" user and "sysadmin" password
    When I navigate to Harbour Interruption Screen
      | From Date  | To Date    | Production Unit | Area     | Facility Class 1 |
      | 2011-01-01 | 2011-01-10 | TS1_EC_PU       | TS1_Area | TS1 Fcty         |
    And I perform insert operation
      | Delay From       | Delay To         | Reason       | Comments |
      | 2011-01-01 00:00 | 2011-01-02 00:00 | *Strong wind | Test     |
    And I perform update operation
      | Reason       | Comments |
      | *Strong wind | Testing1 |
    And I perform delete operation
      | Comments |
      | Testing1 |
    Then verify the record is deleted successfully
    And I logout from application

  @loadurl
  Scenario: This test performs insert, update and delete operations on the Harbour Interruption screen by navigating through URL
    Given I login with "sysadmin" user and "sysadmin" password
    When I navigate to Harbour Interruption Screen using URL
      | From Date  | To Date    | Production Unit | Area     | Facility Class 1 | URL                                                                                                                                                                                                         |
      | 2011-01-01 | 2011-01-10 | TS1_EC_PU       | TS1_Area | TS1 Fcty         | /com.ec.tran.to.screens/harbour_interruption?screentemplate=/com.ec.tran.to.screens/harbour_interruption&CARGO_NAV_CLASS=TERM_OPERATION_NAVIGATOR&TreeviewScreenLabel=Harbour+Interruption&TreeNodeIndex=-2 |
    And I perform insert operation
      | Delay From       | Delay To         | Reason       | Comments |
      | 2011-01-01 00:00 | 2011-01-02 00:00 | *Strong wind | Test     |
    And I perform update operation
      | Reason       | Comments |
      | *Strong wind | Testing1 |
    And I perform delete operation
      | Comments |
      | Testing1 |
    Then IUD performed successfully
    And I logout from application

  @columnindex
  Scenario: Fetching Column Indexes with Column Names
    Given I login with "sysadmin" user and "sysadmin" password
    When I navigate to screen which has table with column headers
      | Production Unit    | Area    | Facility Class 1 |
      | P1 Production Unit | P1 Area | P1 Facility 1    |
    Then All Column Header Indexes with no column parent header should get extracted
      | Total SubHeaders With No Parent | SubHeader Name    |
      |                              12 | Water Mass [kg/d] |
    And All Column Header Indexes with column parent header should get extracted
      | Parent Header Name             | SubHeader Name |
      | Theoretical Calculated Figures | Oil Vol [Sm³]  |
    And All Column Header Indexes with duplicate parent column header should get extracted
      | Parent Header Name | Total SubHeaders Within Duplicate Parent Header |
      | Measured Rates     |                                               4 |
