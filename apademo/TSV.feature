@demo
Feature: Demo to perform IUD using from TSV file

  @readtsv
  Scenario: This test performs insert, update and delete operations on the Harbour Interruption screen in transport module using "TestTSV.tsv"
    Given I login with "sysadmin" user and "sysadmin" password
    When I navigate to Harbour Interruption Screen and load "TestTSV.tsv" for navigation data
    And I perform insert, update and delete operations and load "TestTSV.tsv" for test data
    Then IUD performed successfully using TSV file for test data
    And I logout from application
