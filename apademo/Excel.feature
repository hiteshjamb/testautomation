@demo @readexcel
Feature: Demo to perform IUD using excel file in different format

  Scenario: This test performs insert, update and delete operations on the Harbour Interruption screen in transport module using unique identifier "TestExcel.xlsx"
    Given I login with "sysadmin" user and "sysadmin" password
    And I have the following testdata
      | Workbook       | Sheet Name |
      | TestExcel.xlsx | Navigation |
    When I navigate to Harbour Interruption "Step 01"
    And I perform insert record "TestData" and "Step 02"
    And I perform update record "Step 03"
    And I perform delete record
    And I logout from application
    And I perform insert record "TestData" and "FC1"





  Scenario: This test performs insert, update and delete operations on the Harbour Interruption screen in transport module using "TestExcel.xlsx"
    Given I login with "sysadmin" user and "sysadmin" password
    When I navigate to Harbour Interruption Screen and load "TestExcel.xlsx" for navigation data "navigation"
    And I perform insert, update and delete operations and load "TestExcel.xlsx" for test data "Values"
    Then IUD performed successfully using excel file for test data
    And I logout from application
