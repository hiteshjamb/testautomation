@demo
Feature: Demo to convert file in different formats

  @exceltoxml
  Scenario: Convert Excel to XML
    Given I login with "sysadmin" user and "sysadmin" password
    When I convert the excel to xml
      | Excel File     |
      | TestExcel.xlsx |
    Then I verify the generated xml
      | XML File                      |
      | TestExcel.xlsx_Navigation.xml |
      | TestExcel.xlsx_TestData.xml   |
      | TestExcel.xlsx_Values.xml     |