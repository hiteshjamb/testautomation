@demo @login
Feature: Demo for login to application using different method

  Scenario: Verify login to EC by using plaintext password
    Given I login with "sysadmin" user and "sysadmin" password
    And I logout from application

  Scenario: Verify Login to EC using encrypted password
    Given I login with "sysadmin" user and "xn47GwcKwUt4Y+Sjx906UQ==" as encrypted password
    And I logout from application

  Scenario: Utility to generate encrypted text
    Given I want to view "encrypted" text for "sysadmin" on console

  Scenario: Utility to generate decrypted text
    Given I want to view "decrypted" text for "xn47GwcKwUt4Y+Sjx906UQ==" on console

  Scenario: Verify login to EC by externalizing username and password (encrypted)
    Given I login to EC with valid credentials
    And I logout from application
